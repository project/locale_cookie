<?php

/**
 * @file
 * Administration functions for locale_cookie.module.
 */

/**
 * Identify language from a request/cookie parameter.
 *
 * @param $languages
 *   An array of valid language objects.
 *
 * @return
 *   A valid language code on success, nothing otherwise.
 */
function locale_cookie_language_from_cookie($languages) {
  global $base_path;
  $param = variable_get('locale_cookie_language_negotiation_cookie_param', 'language');

  if (isset($_COOKIE[$param]) && isset($languages[$langcode = $_COOKIE[$param]])) {
    $langcode = $_COOKIE[$param];
    return $langcode;
  }
}